# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


STATES = ('Новый', 'В работе', 'Приостановлен', 'Завершен')

def write_state(apps, schema_editor):
    state = apps.get_model("test_app", "LeadState")
    for s in STATES:
        state.objects.create(name=s)


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Lead',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f', db_index=True)),
            ],
        ),
        migrations.CreateModel(
            name='LeadState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
        ),
        migrations.AddField(
            model_name='lead',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, default=1, verbose_name='\u0421\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u0435', to='test_app.LeadState'),
        ),
        migrations.RunPython(write_state),
    ]
