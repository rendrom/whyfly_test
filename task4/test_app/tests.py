# coding=utf-8
from django.test import TestCase
from test_app.models import Lead, LeadState


class LeadTestCase(TestCase):
    def setUp(self):
        Lead.objects.create(name="test1", state=LeadState.objects.get(id=1))
        Lead.objects.create(name="test2", state=LeadState.objects.get(id=2))
        Lead.objects.create(name="test3", state=LeadState.objects.get(id=3))
        Lead.objects.create(name="test4", state=LeadState.objects.get(id=4))

    def test_lead_state_change(self):
        """Проверка переходов состояния"""
        states = ((), (), (2, 'in_work'), (3, 'paused'), (4, 'completed'))

        def get_disallowed(l):
            return sorted(list(set(range(1, 5)) - set(l.get_allowed())))

        def make_change(lead, x):
            try:
                getattr(lead, states[x][1])()
            except:
                pass

        def check_disallowed(n):
            l = Lead.objects.get(name="test%d" % n.id)
            status = l.state.id
            for m in get_disallowed(l):
                make_change(l, m)
                l = Lead.objects.get(name="test%d" % n.id)
                self.assertEqual(l.state.id, status)

        for o in Lead.objects.all():
            check_disallowed(o)

        for j in [2, 3, 2, 4]:
            lead = Lead.objects.get(name="test1")
            make_change(lead, j)
            lead = Lead.objects.get(name="test1")
            self.assertEqual(lead.state.id, j)

        lead = Lead.objects.get(name="test3")
        make_change(lead, 4)
        lead = Lead.objects.get(name="test3")
        self.assertEqual(lead.state.id, 4)
