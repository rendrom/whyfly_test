# coding=utf-8
from __future__ import unicode_literals
from django.core.exceptions import ValidationError
from django.db import models

STATES = ('Новый', 'В работе', 'Приостановлен', 'Завершен')

class LeadState(models.Model):
    STATE_NEW = 1  # Новый
    STATE_IN_PROGRESS = 2  # В работе
    STATE_POSTPONED = 3  # Приостановлен
    STATE_DONE = 4  # Завершен

    name = models.CharField(
        "Название",
        max_length=50,
        unique=True,
    )

    def __unicode__(self):
        return self.name


class Lead(models.Model):
    name = models.CharField(
        max_length=255,
        db_index=True,
        verbose_name="Имя",
    )
    state = models.ForeignKey(
        LeadState,
        on_delete=models.PROTECT,
        default=LeadState.STATE_NEW,
        verbose_name="Состояние",
    )

    def __init__(self, *args, **kwargs):
        super(Lead, self).__init__(*args, **kwargs)

        def ch(_self, n):
            def func():
                _self.state = LeadState.objects.get(pk=n)
                _self.save()
            return func
        self.__dict__.update(dict(((n, ch(self, i)) for i, n in ((2, 'in_work'), (3, 'paused'), (4, 'completed')))))

    def get_allowed(self):
        return Lead.get_allowed_jumps(self.state.id)

    def save(self, *args, **kwargs):
        if self.pk:
            old = Lead.objects.get(id=self.pk)
            # Если переход не в допустимом диапазоне, то вызывается ошибка
            if self.state.id not in Lead.get_allowed_jumps(old.state.id):
                raise ValidationError('Недопустимый переход состояния: %s > %s' % (old.state.id, self.state.id))
        super(Lead, self).save(*args, **kwargs)

    def __unicode__(self):
        return "{} - {}".format(self.name, self.state)

    @classmethod
    def get_allowed_jumps(cls, n):
        """
        1 Новый -> 2 В работе
        2 В работе -> 3 Приостановлен
        2 В работе -> 4 Завершен
        3 Приостановлен -> 2 В работе
        3 Приостановлен -> 4 Завершен
        """
        return map(int, '2342'[n-1:n+1][0:n]) if n < 4 else []


