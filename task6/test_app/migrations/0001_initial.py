# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import random

from django.db import models, migrations


def insert_entries(apps, schema_editor):
    lead = apps.get_model("test_app", "Lead")
    prod = apps.get_model("test_app", "Product")

    lead_count = 1000

    for i in range(100):
        prod.objects.create(title='product%s' % i, price=random.randrange(10, 100))
    for j in range(lead_count):
        l = lead.objects.create(name='lead%s' % j)
        for x in [random.randrange(1, 100) for i in range(random.randrange(0, 10))]:
            p = prod.objects.get(id=x)
            l.products.add(p)
        l.save()


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Lead',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('price', models.DecimalField(max_digits=20, decimal_places=2)),
            ],
        ),
        migrations.AddField(
            model_name='lead',
            name='products',
            field=models.ManyToManyField(to='test_app.Product'),
        ),
        migrations.RunPython(insert_entries),
    ]
