# coding=utf-8
from django.db import models, connection


class Product(models.Model):
    title = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=20, decimal_places=2)


class Lead(models.Model):
    name = models.CharField(max_length=255)
    products = models.ManyToManyField(Product)

    def get_all_lead_info(self):
        products = ["{0} - {1}".format(product.title, product.price) for product in
                    self.products.all()]
        return u"{name}: {products}".format(name=self.name, products=",".join(products))

    @classmethod
    def get_all_leads_with_short_info(cls):
        return [lead.get_all_lead_info() for lead in Lead.objects.all()]

    @classmethod
    def get_all_leads_with_short_info_sql(cls):
        """Подходит только для sqlite
        """
        cursor = connection.cursor()
        sql = """
            SELECT l.name, group_concat(p.title || ' - ' ||p.price) FROM test_app_lead l
            LEFT OUTER JOIN test_app_lead_products lp ON lp.lead_id = l.id
            LEFT OUTER JOIN test_app_product p ON p.id = lp.product_id
            GROUP BY l.name
            ORDER  BY l.id
        """
        cursor.execute(sql)
        row = cursor.fetchall()
        return ["%s: %s" % (r[0], r[1]) for r in row]

