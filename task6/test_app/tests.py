# coding=utf-8
from __future__ import division
from django.test import TestCase
from test_app.models import Lead
import random
import time


class L:
    def __init__(self, l_str):
        self.l_str = l_str
        self.name, self.prod = self.l_str.split(': ')

    def __eq__(self, other):
        if self.name != other.name:
            return False
        if set(self.products) != set(other.products):
            return False
        return True

    @property
    def products(self):
        return dict((s.split(' - ') for s in self.prod.split(',') if ' - ' in s))



class LeadTestCase(TestCase):
    # def setUp(self):
    #     product_count = 100
    #     for i in range(product_count):
    #         Product.objects.create(title='product%s' % i, price=random.randrange(10, 100))
    #     for j in range(1000):
    #         l = Lead.objects.create(name='lead%s' % j)
    #         for x in [random.randrange(1, 100) for i in range(random.randrange(0, 10))]:
    #             p = Product.objects.get(id=x)
    #             l.products.add(p)
    #         l.save()

    def test_method_(self):
        start = time.time()
        l1 = Lead.get_all_leads_with_short_info()
        d1 = time.time() - start

        start = time.time()
        l2 = Lead.get_all_leads_with_short_info_sql()
        d2 = time.time() - start

        l1_len = len(l1)

        # Проверка эквивалентности между SQL и ORM
        self.assertEqual(l1_len, len(l2))
        r = range(0, l1_len)
        random.shuffle(r)
        for x in r[0:30]:
            self.assertTrue(L(l1[x]) == L(l2[x]))

        print "orm - %s" % d1
        print "SQL - %s" % d2
        if d1 > d2:
            print "SQL быстрее ORM в %.1f раз" % (d1/d2)
        else:
            print 'Х%s...' % ('м'*30)


