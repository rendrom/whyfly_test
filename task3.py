#!/usr/bin/env python
# coding=utf-8
import random
from utils.mytimer import timer
import unittest

task = """
ЗАДАНИЕ:

В ниже представленной функции сортировки есть баг. Укажите, в чем он заключается и
исправьте (при сохранении общего алгоритмического подхода). Покажите
работоспособность исправленной функции через unit тесты.

def sort(lst, beg, end):
    mid = (beg + end) / 2
    sort(lst, beg, mid)
    sort(lst, mid, end)
    i = 0
    j = 0
    for l in range(beg, end):
        if j > end - mid or (i <= mid - beg and lst[beg + i] < lst[mid + j]):
            lst[l] = lst[beg + i]
            i = i + 1
    else:
        lst[l] = lst[mid + j]
        j = j + 1

"""

def sort(lst, beg, end):

    def _sort(lst, beg, end, mid):
        i = 0
        j = 0
        for l in range(beg, end):
            if j > end - mid or (i <= mid - beg and lst[beg + i] < lst[mid + j]):
                lst[l] = lst[beg + i]
                i += 1
            else:
                lst[l] = lst[mid + j]
                j += 1
    mid = (beg + end) / 2
    for x in range(beg, mid):
        mid_x = (x + mid) / 2
        _sort(lst, x, mid, mid_x)
    for x in range(mid, end):
        mid_x = (x + end) / 2
        _sort(lst, x, end, mid_x)
    _sort(lst, beg, end, mid)


def sort_slice(lst, beg=None, end=None):
    beg = beg or 0
    end = end or len(lst)
    lst[beg:end] = sorted(lst[beg:end])

def answer():
    print """
    РЕШЕНИЕ:
    В этом задании предлагается устранить баг в функции сортировки,
    сохранив общий алгоритмический подход.
    Было установлено, что самым явным "багом", который останавливает выполнение функции,
    является наличие "рекурсивной петли". Для преодоления этой ошибки вызовы рекурсивной
    функции были заменены на циклы, с вызовом новой локальной функции, в которую был перенесён
    весь алгоритм исходного метода сортировки.

    def sort(lst, beg, end):

        def _sort(lst, beg, end, mid):
            i = 0
            j = 0
            for l in range(beg, end):
                if j > end - mid or (i <= mid - beg and lst[beg + i] < lst[mid + j]):
                    lst[l] = lst[beg + i]
                    i += 1
                else:
                    lst[l] = lst[mid + j]
                    j += 1
    mid = (beg + end) / 2
    for x in range(beg, mid):
        mid_x = (x + mid) / 2
        _sort(lst, x, mid, mid_x)
    for x in range(mid, end):
        mid_x = (x + end) / 2
        _sort(lst, x, end, mid_x)
    _sort(lst, beg, end, mid)

    К сожалению, дальнейший поиск "багов" не привёл ни к каким результатам. Возникли трудности с
    пониманием алгоритма сортировки. При попытках выполнить метод "как есть" исходный список
    претерпевает значительные изменения, но без какого-либо перераспределения, ожидаемого от
    выполнения сортировки.
    Исходя из того, что функция называется sort и имеет такие значения аргументов
    как begin и end, была выдвинута гипотеза, что это функция - сортировка среза.
    Придерживаясь данной версии были выполнены дальнейшие операции тестирования для:

    def sort_slice(lst, beg=None, end=None):
        beg = beg or 0
        end = end or len(lst)
        lst[beg:end] = sorted(lst[beg:end])

    """

class TestSortethods(unittest.TestCase):
    def test_sort(self):
        """Сортировка выполняется и производит изменения в исходном списке
        """
        r1 = range(100)
        r2 = r1[:]
        sort(r2, 0, 50)
        self.assertTrue(r1 != r2)

    def test_sort_slice(self):
        """Тестирование сортировки заданного среза списка
        """
        def check(r, m):
            x = r[0]
            er = False
            for i in range(1, m):
                er = r[i] < x
                x = r[i]
            self.assertTrue(not er)
        r = range(100, 0, -1)
        sort_slice(r, 0, 50)
        check(r, 50)
        r.reverse()
        sort_slice(r)
        check(r, 100)


if __name__ == '__main__':
    print task
    answer()
    unittest.main()
