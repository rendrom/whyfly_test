#!/usr/bin/env python
# coding=utf-8
from utils.mytimer import timer

task = """
    ЗАДАНИЕ:

    В ниже представленном коде (Python 2.x) есть проблема.
    Найдите её, охарактеризуйте и объясните ваше исправление.

    print reduce(lambda x, y: x + y,
                 filter(lambda x: x % 2,
                        map(lambda x: x * x, xrange(10 ** 6)))) = \\
          sum(x * x for x in xrange(1, 10 ** 6, 2))
    \n
"""


def answer():
    print """
    РЕШЕНИЕ:

    Ошибкой в данном коде является использование оператора присваивания " = "
    в том месте, где должно быть сравнение " == ".
    Исправленный код функции выглядит следующим образом:

    print reduce(lambda x, y: x + y,
                 filter(lambda x: x % 2,
                        map(lambda x: x * x, xrange(10 ** 6)))) == \\
          sum(x * x for x in xrange(1, 10 ** 6, 2))

    Сравниваются две инструкции, выполняющие эквивалентные действия.
    При этом запись инструкции в правой части выражения является более оптимальной,
    в то время, как левая часть избыточная и ведет к заметному отставанию в производительности.

    Рассмотрим время выполнения левой и правой части выражения по отдельности:
    """

    def left_part():
        reduce(lambda x, y: x + y,
               filter(lambda x: x % 2,
                      map(lambda x: x * x, xrange(10 ** 6))))

    def right_part():
        sum(x * x for x in xrange(1, 10 ** 6, 2))

    left_time = timer(left_part)
    right_time = timer(right_part)

    print """
    левая: {0}
    правая: {1}
    """.format(left_time, right_time)


if __name__ == '__main__':
    print task
    answer()
