#!/usr/bin/env python
# coding=utf-8
import sys
from utils.mytimer import timer
import unittest

task = """
    ЗАДАНИЕ:

    Реализуйте функцию, которая будет генерировать последовательность Фибоначчи
    заданной длины. Реализуйте рекурсивный и не рекурсивный варианты. Можно ли
    реализовать рекурсивный вариант для получения последовательности Фибоначчи
    произвольной длины и почему?\n
    """


def fib_gen(n):
    f1, f2 = 0, 1
    for x in range(0, n):
        yield f1
        f1, f2 = f2, f1 + f2


def fib_loop(n):
    f1, f2 = 0, 1
    l = []
    for x in range(0, n):
        l.append(f1)
        f1, f2 = f2, f1 + f2
    return l


def fib_r(n):
    mem = [0] * n

    def fib(m):
        if m > 1:
            mem[m - 1] = fib(m - 1)
            return sum(mem[m - 2:m])
        return m
    fib(n)
    return mem


def answer():
    print """
    РЕШЕНИЕ:

    'Последовательность Фибоначчи образует ряд, в котором каждое последующее число
    равно сумме двух предыдущих.'

    В этом задании были реализованы три функции, позволяющие выводить последовательность Фибоначчи

    1. На основе цикла for:

    def fib_loop(n):
        f1, f2 = 0, 1
        l = []
        for x in range(0, n):
            l.append(f1)
            f1, f2 = f2, f1 + f2
        return l

    fib_loop(42)

    2. С использованием генератора

    def fib_gen(n):
        f1, f2 = 0, 1
        for x in range(0, n):
            yield f1
            f1, f2 = f2, f1 + f2

    list(fib_gen(42))

    for f in fib_gen(42):
        pass

    3. Рекурсивный подход:

    def fib_r(n):
        mem = [0] * n

        def fib(m):
            if m > 1:
                mem[m - 1] = fib(m - 1)
                return sum(mem[m - 2:m])
            return m
        fib(n)
        return mem

    fib_r(42)

    Для работе с рекурсивной реализацией было принято решение добавить оптимизацию
    за счет записи промежуточных значений в память.

    Рассмотрим скорость выполнения всех трёх функций
    (будет взято общее время выполнения 1000 вызовов каждой функции):
    """
    loop = timer(fib_loop, 500, n=1000)
    gen = timer(lambda: list(fib_gen(500)), n=1000)
    rec = timer(fib_r, 500, n=1000)
    print "\tfib_loop : %s" % loop
    print "\tfib_gen  : %s" % gen
    print "\tfib_r    : %s" % rec

    gen_lead = ''
    if gen < loop:
        gen_lead =  "\b(использование генераторов быстрее всего на %s сек)" % (loop-gen)
    print """
    Как мы видим, разница между fib_loop и fib_gen незначительна{}, а вот
    рекурсивный подход fib_r затрачивает горазда больше времени
    """.format(gen_lead)

    print """
    Помимо медлительности реурсивный подход обладает (в языке программирования Python)
    ограничением глубины в {} вызовов, при несоблюдении которого будет выведено сообщение
    об ошибке (RuntimeError: maximum recursion depth exceeded)
    """.format(sys.getrecursionlimit())


class TestFibMethods(unittest.TestCase):
    def test_fib_compare(self):
        """Проверка эквивалентности результатов выполнения трёх
           разных функций вывода последовательности Фибоначи
        """
        n = 100
        loop, gen, rec = fib_loop(n), list(fib_gen(n)), fib_r(n)
        self.assertEqual(loop, gen)
        self.assertEqual(gen, rec)

    def test_fib_recursion_limit(self):
        """Проверка существующих ограничений на рекурсии в Python
        """
        err = False
        try:
            fib_r(1001)
        except:
            err = True
        self.assertTrue(err)


if __name__ == '__main__':
    print task
    answer()
    unittest.main()
