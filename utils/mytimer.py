import time
import sys

if sys.platform[:3] == 'win':
    timefunc = time.clock
else:
    timefunc = time.time


def timer(func, *args, **kwargs):
    n = kwargs.pop('n', 1)
    nlist = range(n)
    start = timefunc()
    for i in nlist:
        func(*args, **kwargs)
    elapsed = timefunc() - start
    return elapsed

